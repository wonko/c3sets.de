---
title: "[gpn19] Gecko Biloba - DJ Set"
date: 2023-10-11T09:59:34+02:00
playdate: 2019-06-01T16:00:00+02:00
artists: ['gecko']
events: ['gpn19']
draft: false
# alternative link: https://archive.org/details/geckosmixtapes/2019-06-01-gpn19-cosmogecko.mp3
---

https://soundcloud.com/geckobiloba/gpn19-dj-set-by-cosmogecko