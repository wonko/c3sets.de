---
title: "[37c3] Nik - @Maskenball"
date: 2024-01-23T08:19:44+01:00
playdate: 2023-12-28T05:00:00+01:00
artists: ['Nik']
events: ['37c3']
draft: false
---

[320kbps MP3 (136 MB)](https://duskybucket.s3.eu-central-1.amazonaws.com/2023-12-28%2037C3%20Maskenball.mp3)

[FLAC (829 MB)](https://duskybucket.s3.eu-central-1.amazonaws.com/2023-12-28%2037C3%20Maskenball.flac)

https://www.mixcloud.com/NikTheDusky/nik-37c3-maskenball-toilet-rave/
